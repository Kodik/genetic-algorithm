class GeneticAlgorithm
  # Function: ax^3+3bx^2+2ln(x+1)
  Individual = Struct.new(:chromosome, :phenotype, :value, :percent)

  CROSSBREEDING_PROBABILITY = 0.85
  MUTATION_PROBABILITY = 0.1
  ITERATIONS_NUMBER = 50
  NUMBER_OF_INDIVIDUALS = 40

  def perform
    ITERATIONS_NUMBER.times do
      iterate
    end
    the_best = find_the_best_individual
    puts "\nNajwiększa wartość funkcji: #{the_best.value} dla osobników z chromosomem #{the_best.chromosome} oraz fenotypem #{the_best.phenotype}"
  end

  def iterate
    prepare_roulette_wheel
    draw_new_individuals
    crossbreed
    mutate
    update_phenotypes
    update_function_values
  end

  private

  attr_reader :a, :b, :individuals, :wheel

  def initialize(a:, b:)
    @a = a
    @b = b
    @individuals = {}
    @wheel = {}
    draw_chromosomes
  end

  def draw_chromosomes
    NUMBER_OF_INDIVIDUALS.times do |number|
      phenotype = rand(0..256)
      chromosome = phenotype.to_s(2).rjust(9, '0')
      function_value = calculate_function(phenotype)
      individuals[number + 1] = Individual.new(chromosome, phenotype, function_value)
    end
  end

  def find_the_best_individual
    individual = individuals[1]
    individuals.each do |k, v|
      individual = v if v.value > individual.value
    end
    individual
  end

  def prepare_roulette_wheel
    to_add = how_many_add
    individuals.each do |_k, i|
      i.percent = calculate_percent(i.value, sum_of_function_values, to_add)
    end
    sum_before = 0
    sum_after = 0
    individuals.each do |k, i|
      percent = i.percent.round(10)
      case k
      when 1
        sum_after = (sum_after + percent).round(10)
        wheel[k] = (0.0..sum_after)
        sum_before = sum_after
      when 1...NUMBER_OF_INDIVIDUALS
        sum_after = (sum_before + percent).round(10)
        sum_before = (sum_before + 0.0000000001).round(10)
        wheel[k] = (sum_before..sum_after)
        sum_before = sum_after
      when NUMBER_OF_INDIVIDUALS
        sum_before = (sum_before + 0.0000000001).round(10)
        wheel[k] = (sum_before..100.0)
      end
    end
  end

  def draw_new_individuals
    individuals_copy = individuals.dup
    NUMBER_OF_INDIVIDUALS.times do |number|
      random_number = rand(0.0..100.0).round(10)
      n = number + 1
      individual_number = wheel.detect { |_k, v| v.include?(random_number) }.first
      individuals[n].chromosome = individuals_copy[individual_number].chromosome.clone
      individuals[n].phenotype = individuals[n].chromosome.to_i(2)
    end
  end

  def crossbreed
    NUMBER_OF_INDIVIDUALS.times do |n|
      next if n.even?
      next unless rand(0.0..1.0).round(2) < CROSSBREEDING_PROBABILITY
      first_individual = individuals[n]
      second_individual = individuals[n + 1]
      first_chromosome = first_individual.chromosome
      second_chromosome = second_individual.chromosome
      crossbreeding_point = rand(1..8)
      first_individual.chromosome =
        first_individual.chromosome.slice(0..(crossbreeding_point - 1)) +
        second_chromosome.slice(crossbreeding_point..8)
      second_individual.chromosome =
        second_individual.chromosome.slice(0..(crossbreeding_point - 1)) +
        first_chromosome.slice(crossbreeding_point..8)
    end
  end

  def mutate
    NUMBER_OF_INDIVIDUALS.times do |n|
      next unless rand(0.0..1.0).round(2) < MUTATION_PROBABILITY
      mutation_point = rand(0..8)
      chromosome = individuals[n + 1].chromosome
      if chromosome[mutation_point] == '0'
        chromosome[mutation_point] = '1'
      else
        chromosome[mutation_point] = '0'
      end
      individuals[n + 1].chromosome = chromosome
    end
  end

  def update_phenotypes
    individuals.each do |_k, v|
      v.phenotype = v.chromosome.to_i(2)
    end
  end

  def update_function_values
    individuals.each do |_k, v|
      v.value = calculate_function(v.phenotype)
    end
  end

  def calculate_function(x)
    a * x**3 + 3 * b * x**2 + 2 * Math.log(x + 1)
  end

  def calculate_percent(value, sum_of_function_values, to_add)
    ((value + to_add[:to_value]) / (sum_of_function_values + to_add[:to_sum]) * 100)
  end

  def sum_of_function_values
    sum = 0
    individuals.each { |_k, i| sum += i.value }
    sum
  end

  def how_many_add
    smalest_number = nil
    to_value = 0
    to_sum = 0
    individuals.each do |_k, i|
      next unless i.value.negative?
      if smalest_number.nil?
        smalest_number = i.value
      else
        smalest_number = i.value if smalest_number > i.value
      end
    end

    unless smalest_number.nil?
      to_value = smalest_number * -1 + 1
      to_sum = to_value * NUMBER_OF_INDIVIDUALS
    end

    { to_value: to_value, to_sum: to_sum }
  end

  # def log_individuals
  #   individuals.each do |k, i|
  #     string = "ch#{k}".rjust(4, ' ') +
  #               " = #{i.chromosome}(#{i.chromosome.object_id})" +
  #               ' | ' +
  #               "ch#{k}*".rjust(5, ' ') +
  #               " = #{i.phenotype.to_s.rjust(3, ' ')} | " +
  #               "v: #{i.value}\n"
  #     file.write(string)
  #   end
  # end

    # def print_chromosomes
  #   puts "\nPopulacja:"
  #   individuals.each do |k, i|
  #     puts "ch#{k}".rjust(4, ' ') +
  #       " = #{i.chromosome}" +
  #       ' | ' +
  #       "ch#{k}*".rjust(5, ' ') +
  #       " = #{i.phenotype}"
  #   end
  # end
end

class Program
  def call
    puts ' *** Algorytm Genetyczny *** '
    puts 'Funkcja przystosowania: ax^3+3bx^2+2ln(x+1)'
    puts "Liczba iteracji: #{GeneticAlgorithm::ITERATIONS_NUMBER}"
    puts "Prawdopodobieństwo krzyżowania: #{GeneticAlgorithm::CROSSBREEDING_PROBABILITY}"
    puts "Prawdopodobieństwo mutacji: #{GeneticAlgorithm::MUTATION_PROBABILITY}"

    print "Podaj a: "
    a = gets.chomp.to_i
    print "Podaj b: "
    b = gets.chomp.to_i

    algorithm = GeneticAlgorithm.new(a: a, b: b)
    algorithm.perform
  end
end

Program.new.call
